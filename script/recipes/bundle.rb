bash "bundle_install" do
  user "root"
  cwd "/data/www/html/api.shoplineapp.com"
  code <<-EOH
    exec > >(tee /var/log/bundle.log|logger -t user-data -s 2>/dev/console) 2>&1
    source /usr/local/rvm/scripts/rvm # make sure using the ruby from rvm
    bundle
  EOH
end
